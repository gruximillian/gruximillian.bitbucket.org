(function() {

    Date.prototype.addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    var startQuiz = document.getElementById('quiz-start'),
        quizNext = document.getElementById('quiz-next'),
        quizPrevious = document.getElementById('quiz-previous'),
        resultNode = document.getElementById('quiz-result'),
        questionForm = document.getElementById('quiz-question'),
        quizWarning = document.getElementById('quiz-answer-warning'),
        loginForm = document.forms['quiz-login'],
        loginButton = loginForm.elements['login-button'],
        logoutButton = loginForm.elements['logout-button'],
        registerButton = loginForm.elements['register-button'],
        registerForm = document.forms['quiz-register'],
        submitRegister = registerForm.elements['submit-register-button'],
        cancelRegister = registerForm.elements['cancel-register-button'],
        registerOverlay = document.getElementById('quiz-register-overlay'),
        regWarning = document.getElementById('register-warning');

    var Event = {

        add: function(element, type, handler) {
            if (element.addEventListener) {
                element.addEventListener(type, handler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + type, handler);
            } else {
                element['on' + type] = handler;
            }
        },

        remove: function(element, type, handler) {
            if (element.removeEventListener) {
                element.removeEventListener(type, handler, false);
            } else if (element.detachEvent) {
                element.detachEvent('on' + type, handler);
            } else {
                element['on' + type] = null;
            }
        }

    };

    var quiz = function() {

        return {

/****************************************************/
/* Setting these properties is not needed since     */
/* they are set by the resetquiz and startQuiz      */
/* methods. But they are listed here, in one place, */
/* so that the developer is aware of their usage.   */
/****************************************************/

            playerName: '',

            playerScore: 0,

            playerAnswers: [],

            currentQuestion: 0,

            allQuestions: [],

/****************************************************/

            welcomeMessage: 'Welcome to \'Javascript sQuiz\' quiz!\nEnter your name to start.',

            userLoggedIn: function(username) {
                if (username && window.localStorage && !!localStorage.getItem('quizUsers')) {

                    users = JSON.parse(localStorage.getItem('quizUsers'));

                    if (users[username]) {
                        return true;
                    }
                }
                return false;
            },

            getQuestions: function () {
                $.ajax({
                    type: 'GET',
                    url: 'data/questions01.json',
                    context: this,
                    dataType: 'json'
                }).done(function (questions) {
                    this.allQuestions = questions;
                    this.startQuiz();
                });
            },

            getUserName: function () {
                var playerName = prompt(this.welcomeMessage);
                if (playerName === null) return null;
                if (playerName !== '') {
                    Cookie.set('name', playerName, new Date());
                } else playerName = 'Anonymous';
                this.playerName =  playerName;
                return playerName;
            },

            startQuiz: function () {
                var playerName,
                    currentQuestionObject = this.allQuestions[0],
                    cookieValue = Cookie.get('name'),
                    oldUser;

                resultNode.innerHTML = '';
                if (cookieValue && !this.userLoggedIn(this.playerName)) {
                    this.playerName = cookieValue;
                    playerName = cookieValue;
                    oldUser = confirm('Welcome back ' + cookieValue + '! Let me ask you some questions. :)\nNot ' + cookieValue + '? Click Cancel to enter your name.');
                    if (!oldUser) {
                        playerName = this.getUserName();
                        if (playerName === null) return;
                    }
                } else if (!this.userLoggedIn(this.playerName)) {
                    playerName = this.getUserName();
                    if (playerName === null) return;
                } else {
                    playerName = this.playerName;
                }

                this.allQuestions[0].choices[2] = playerName;
                startQuiz.classList.toggle('hide');
                questionForm.innerHTML = this.createQuestion(currentQuestionObject);
                $(questionForm).fadeIn(300);
                quizNext.classList.toggle('hide');
                quizPrevious.classList.remove('hide');
            },

            createQuestion: function (question) {
                var questionText = question.question,
                    choices = question.choices,
                    numOfChoices = choices.length,
                    questionHTML = [],
                    checked = '',
                    answer;

                answer = this.playerAnswers[this.currentQuestion];

                questionHTML.push('<p><strong>' + questionText + '</strong></p>');
                for (var i = 0; i < numOfChoices; i++) {
                    checked = (answer === i) ? 'checked' : '';
                    questionHTML.push('<p><input type="radio" id="answer_' + i + '" name="choice" value="' + choices[i] + '" ' + checked +'><label for="answer_' + i + '"> ' + choices[i] + '</label></p>');
                }

                return questionHTML.join('');
            },

            changeQuestion: function (html) {
                $(questionForm).fadeOut(100, function () {
                    questionForm.innerHTML = html;
                }).fadeIn(300);
            },

            isAnswered: function () {
                var answerChoices = document.getElementsByName('choice');
                answerChoices = Array.prototype.slice.call(answerChoices); // Converts NodeList to Array, needed for .some() method

                function checked(item, index, array) {
                    if (item.checked) {
                        this.playerAnswers[this.currentQuestion] = index;
                        return true;
                    }
                };

                return answerChoices.some(checked, this);
            },

            nextQuestion: function () {
                var questionNum = this.currentQuestion + 1,
                    currentQuestionObject = this.allQuestions[questionNum],
                    totalQuestions = this.allQuestions.length,
                    questionHTML,
                    answerWarning = $(quizWarning),
                    answerWarningText = answerWarning.find('p');

                if (this.isAnswered()) { // If true, the question is answered

                    if (questionNum < totalQuestions) {
                        this.currentQuestion = questionNum;
                        questionHTML = this.createQuestion(currentQuestionObject);
                        this.changeQuestion(questionHTML);
                        if (this.currentQuestion + 1 === totalQuestions) {
                            quizNext.innerHTML = 'Finish!';
                        }
                    } else {
                        questionForm.innerHTML = '';
                        this.calculateResult();
                        this.showResult();
                    }
                } else {
                    answerWarningText.css('top', answerWarning.height() / 2 - 50);
                    answerWarning.fadeIn(300).fadeOut(1200); // Warning when question is not answered
                }

                if (this.currentQuestion > 0) {
                    quizPrevious.removeAttribute('disabled');
                }
            },

            previousQuestion: function () {
                if (this.currentQuestion > 0){
                    var questionNum = this.currentQuestion - 1,
                        previousQuestionObject = this.allQuestions[questionNum],
                        totalQuestions = this.allQuestions.length,
                        questionHTML;

                    this.isAnswered();
                    this.currentQuestion = questionNum;
                    questionHTML = this.createQuestion(previousQuestionObject);
                    this.changeQuestion(questionHTML);
                    if (this.currentQuestion + 1 < totalQuestions) {
                        quizNext.innerHTML = 'Submit and go to the next question';
                    }
                    if (questionNum === 0) {
                        quizPrevious.setAttribute('disabled', true);
                    }
                }
            },

            calculateResult: function () {
                var questions = this.allQuestions,
                    answers = this.playerAnswers,
                    score = 0;

                questions.forEach(function(question, index) {
                    score = (question.correctAnswer === answers[index]) ? score + 1 : score;
                });
                this.playerScore = score;
            },

            showResult: function () {
                var score = this.playerScore,
                    username = this.playerName,
                    users,
                    user;

                if (this.userLoggedIn(username)) {
                    users = JSON.parse(localStorage.getItem('quizUsers'));
                    user = users[username];
                    if (user.bestscore < score) {
                        user.bestscore = score;
                        $('#best-score').text(score);
                        localStorage.setItem('quizUsers', JSON.stringify(users));
                    }
                }

                resultNode.innerHTML = 'Number of correct answers: ' + score + ' out of ' + this.allQuestions.length;
                startQuiz.innerHTML = 'Start again?';
                this.resetQuiz();
            },

            resetQuiz: function () {
                $(questionForm).hide();
                this.currentQuestion = 0;
                this.playerScore = 0;
                this.playerAnswers = [];
                quizPrevious.setAttribute('disabled', true);
                quizNext.innerHTML = 'Submit and go to the next question';
                quizPrevious.classList.add('hide');
                quizNext.classList.add('hide');
                startQuiz.classList.remove('hide');
            }
        }

    }();

    var User = {

        login: function (e) {
            //console.log('Logging in!');
            e.preventDefault();
            if (window.localStorage) {
                var username = loginForm.elements['username'].value,
                    password = loginForm.elements['password'].value,
                    users,
                    user,
                    register;

                if (!username || !password) return;

                if (!localStorage.getItem('quizUsers')) {
                    users = {};
                    localStorage.setItem('quizUsers', JSON.stringify(users));
                }

                users = JSON.parse(localStorage.getItem('quizUsers'));
                user = users[username];

                if (user) {

                    if (user.password === password) {
                        //console.log('Ulogovan!');
                        this.showLoggedUser(user);
                        quiz.resetQuiz();
                    } else {
                        alert('Wrong login information. Please, try again.');
                        return;
                    }

                } else {
                    register = confirm('User "' + username + '" does not exist. Would you like to register?');
                    if (!register) return;
                    this.showRegisterForm();
                }

            } else {
                alert('Cannot login! LocalStorage not enabled.');
            }
        },

        logout: function (e) {
            //console.log('Logging out!');
            e.preventDefault();
            quiz.playerName = '';
            quiz.resetQuiz();
            $('.login-data').show();
            $('.logout-data').hide();
            $(registerButton).show();
            $('#logged-user').empty().hide();
            loginForm.reset();
        },

        showLoggedUser: function (user) {
            var userInfo = 'Hi <strong>' + user.username + '</strong>, your best score is <strong id="best-score">' + user.bestscore + '</strong>';
            $('.login-data').hide();
            $('.logout-data').show();
            $(registerButton).hide();
            $('#logged-user').append(userInfo).show();
            quiz.playerName = user.username;
        },

        showRegisterForm: function () {
            $(registerOverlay).fadeIn(300);
            $('.login-data').prop('disabled', true);
        },

        hideRegisterForm: function () {
            $(registerOverlay).fadeOut(300, function() {
                registerForm.reset();
                $('.login-data').prop('disabled', false);
            });
        },

        register: function (e) {
            //console.log('Registering!');
            e.preventDefault();

            if (window.localStorage) {
                var username = registerForm.elements['username'].value,
                    password1 = registerForm.elements['password1'].value,
                    password2 = registerForm.elements['password2'].value,
                    users,
                    user;

                if (!localStorage.getItem('quizUsers')) {
                    users = {};
                    localStorage.setItem('quizUsers', JSON.stringify(users));
                }

                users = JSON.parse(localStorage.getItem('quizUsers'));
                user = users[username];

                if (user) {
                    alert('User already exists! Try another username.');
                    return;
                } else if (username && password1 && password2) {
                    if (password1 !== password2) {
                        alert('Passwords don\'t match! Try Again.');
                        return;
                    }
                    users[username] = {};
                    users[username].username = username;
                    users[username].password = password1;
                    users[username].bestscore = 0;
                    user = users[username];
                    localStorage.setItem('quizUsers', JSON.stringify(users));
                    this.hideRegisterForm();
                    this.showLoggedUser(user);
                    quiz.resetQuiz();
                } else {
                    alert('Enter username and password!');
                }

            } else {
                alert('Cannot register! LocalStorage not enabled.');
            }
        }

    };

    var Cookie = {
        set: function (name, value, date) {
            var cookieValue;

            cookieValue = encodeURIComponent(name) + '=' + encodeURIComponent(value);
            if (date instanceof Date) {
                var expires = date.addDays(7);
                cookieValue += '; expires=' + expires.toGMTString();
                //console.log('expires: ' + expires.toGMTString());
            }

            document.cookie = cookieValue;
        },

        get: function (name) {
            var cookieName = encodeURIComponent(name) + '=',
                cookieStart = document.cookie.indexOf(cookieName),
                cookieValue = null,
                cookieEnd;

            if (cookieStart > -1) {
                cookieEnd = document.cookie.indexOf(';', cookieStart);
                if (cookieEnd === -1) {
                    cookieEnd = document.cookie.length;
                }
                cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length, cookieEnd));
            }

            return cookieValue;
        }
    };

    Event.add(startQuiz, 'click', quiz.getQuestions.bind(quiz));
    Event.add(quizNext, 'click', quiz.nextQuestion.bind(quiz));
    Event.add(quizPrevious, 'click', quiz.previousQuestion.bind(quiz));
    Event.add(loginButton, 'click', User.login.bind(User));
    Event.add(logoutButton, 'click', User.logout.bind(User));
    Event.add(registerButton, 'click', User.showRegisterForm.bind(User));
    Event.add(submitRegister, 'click', User.register.bind(User));
    Event.add(cancelRegister, 'click', User.hideRegisterForm.bind(User));
    Event.add(regWarning, 'mouseover', function() {
        $('#warning-text').show();
    });
    Event.add(regWarning, 'mouseout', function() {
        $('#warning-text').hide();
    });

})();
